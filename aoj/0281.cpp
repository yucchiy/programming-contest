#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    int Q;
    cin >> Q;
    while (Q-- > 0) {
        int c, a, n, ans = 0;
        cin >> c >> a >> n;
        // 1 1 1
        for (int i = 0; i <= n; i++) {
            // 2 1 0
            for (int j = 0; j <= (a - i); j++) {
                // 3 0 0
                for (int k = 0; k <= (c - i - 2 * j) / 3; k++) {
                    int c_ = i + 2 * j + 3 * k,
                        a_ = i + j,
                        n_ = i;
                    if (c_ <= c && a_ <= a && n_ <= n) ans = max(ans, i + j + k);
                }
            }
        }

        cout << ans << endl;
    }

    return 0;
}

