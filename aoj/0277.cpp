#include <iostream>

using namespace std;

int price[4] = {6000, 4000, 3000, 2000};

int main()
{
    int t, n;
    for (int i = 0; i < 4; i++) {
        cin >> t >> n;
        cout << price[t - 1] * n << endl;
    }

    return 0;
}
