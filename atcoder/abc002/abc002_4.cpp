#include <iostream>

using namespace std;

int n, m, N = 12;
bool visited[13], connected[13][13];

void rec(int i)
{
    if (visited[i]) return;
    visited[i] = true;

    for (int j = 1; j <= N; j++) {
        if (connected[i][j] == 1) {
            rec(j);
        }
    }
}

int main()
{
    for (int i = 0; i <= N; i++) {
        visited[i] = false;
        for (int j = 0; j <= N; j++) {
            connected[i][j] = false;
        }
    }

    cin >> n >> m;
    for (int i = 0; i < m; i++) {
        int x, y;
        cin >> x >> y;
        connected[x][y] = connected[y][x] = true;
    }

    int res = 0;
    for (int i = 1; i <= n; i++) {
        if (visited[i] == false) {
            res++;
            rec(i);
        }
    }

    cout << res << endl;
}
