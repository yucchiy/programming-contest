#include <iostream>

using namespace std;

int main()
{
    int N, x, y, b, p;
    cin >> N;
    for (int d = 0; d < N; d++) {
        cin >> x >> y >> b >> p;
        int ans = min(b * x + p * y, (int)(max(b, 5) * x * 0.8) + (int)(max(p, 2) * y * 0.8));

        cout << ans << endl;
    }
}

