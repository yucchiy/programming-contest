#include <iostream>

using namespace std;

int main()
{
    char c;
    int k, timing[10];
    for (int i = 0; i < 10; i++) timing[i] = 0;

    cin >> k;
    for (int i = 0; i < 16; i++) {
        cin >> c;
        if (c == '.') continue;
        timing[c - '0']++;
        if (timing[c - '0'] > (k * 2)) {
            cout << "NO" << endl;
            return 0;
        }
    }
    cout << "YES" << endl;
}
