#include <iostream>
#include <cstring>
#include <vector>
#include <queue>

using namespace std;

int dx[] = {0, 1, 0, -1}, dy[] = {-1, 0, 1, 0};

int main()
{
    int n, m, k, sx, sy = -1;
    char maze[500][500];
    cin >> n >> m >> k;
    for (int y = 0; y < n; y++) {
        for (int x = 0; x < m; x++) {
            cin >> maze[y][x];
            if (maze[y][x] == '.' && sy == -1) {
                sx = x; sy = y;
            }
        }
    }

    bool visited[500][500];
    vector< pair<int, int> > points;
    queue< pair<int, int> > que;

    que.push(pair<int, int>(sx, sy));
    visited[sy][sx] = true;
    while(!que.empty()) {
        pair<int, int> pos = que.front();
        que.pop();
        for (int i = 0; i < 4; i++) {
            int nx = pos.first + dx[i];
            int ny = pos.second + dy[i];
            if (nx < 0 || ny < 0 || nx >= m || ny >= n || maze[ny][nx] != '.' || visited[ny][nx]) continue;

            que.push(pair<int, int>(nx, ny));
            points.push_back(pair<int, int>(nx, ny));
            visited[ny][nx] = true;
        }
    }

    for (int i = 0; i < k; i++) {
        pair<int, int> pos = points[points.size() - i - 1];
        maze[pos.second][pos.first] = 'X';
    }

    for (int y = 0; y < n; y++) {
        for (int x = 0; x < m; x++) {
            cout << maze[y][x];
        }
        cout << endl;
    }
}