#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

int main()
{
    int n, a, b;
    vector< pair<int, int> > all;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a >> b;
        all.push_back(pair<int, int>(a, i));
        all.push_back(pair<int, int>(b, i + n));
    }

    sort(all.begin(), all.end());
    int chance[2][10001];
    for (int i = 0; i < n; i++) {
        cout << all[i].first << " " << all[i].second  << endl;
        chance[all[i].second / n][all[i].second % n] = 1;
    }

    for (int i = 0; i < n; i++) {
        if (i < (int)floor(n / 2)) {
            cout << 1;
        } else {
            cout << chance[0][i];
        }
    }
    cout << endl;

    for (int i = 0; i < n; i++) {
        if (i < (int)floor(n / 2)) {
            cout << 1;
        } else {
            cout << chance[1][i];
        }
    }
    cout << endl;
}