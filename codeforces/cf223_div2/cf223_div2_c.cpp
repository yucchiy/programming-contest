#include <iostream>
#include <vector>
#include <set>

typedef long long ll;

using namespace std;

int main()
{
    int m, n, t, l, c, num;
    vector<int> seq;
    set<int> st;
    cin >> m;
    for (int i = 0; i < m; i++) {
        cin >> t;
        if (t == 1) {
            cin >> num;
            seq.push_back(num);
        } else {
            cin >> l >> c;
            for (int j = 0; j < c; j++) {
                for (int k = 0; k < l; k++) {
                    seq.push_back(seq[k]);
                }
            }
        }
    }

    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> num;
        st.insert(num);
    }

    bool first = true;
    for (int i = 0; i < seq.size(); i++) {
        if (st.find(seq[i]) != st.end()) {
            if (first) {
                cout << seq[i];
                first = false;
            } else {
                cout << " " << seq[i];
            }
        }
    }

    cout << endl;

    return 0;
}
