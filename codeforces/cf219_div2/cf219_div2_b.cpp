#include <iostream>
#include <cmath>

using namespace std;

typedef long long ll;

int main()
{
    ll w, m, k;
    cin >> w >> m >> k;
    w /= k;

    ll cost = 0, ans = 0, digit = 1, pow = 10;
    while (pow < m) {
        pow = pow * 10;
        digit++;
    }

    while (true) {
        ll num = pow - m;
        if (w / digit >= num) {
            ans = ans + num;
            w = w - (digit * num);
            m = pow;
            pow = pow * 10;
            digit++;
        } else {
            ll x = w / digit;
            ans = ans + x;
            break;
        }
    }

    cout << ans << endl;
}
