#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>

using namespace std;

int main()
{
    int m, b, cnt[100001] = {0};
    cin >> m;

    for (int i = 0; i < m; i++) { 
        cin >> b;
        cnt[b]++;
    }

    int ans = 0;
    vector<int> seq;
    for (int i = 1; i <= 100000; i++) {
        if (cnt[i] > 1) {
            seq.push_back(i);
        }
    }

    bool first = true;
    for (int i = 100000; i > 0; i--) {
        if (cnt[i] > 0) {
            if (first) {
                if (cnt[i] == 1) {
                    seq.push_back(i);
                }
                first = false;
            } else {
                seq.push_back(i);
            }
        }
    }

    cout << seq.size() << endl;
    cout << seq[0];

    for (int i = 1; i < seq.size(); i++) {
        cout << " " << seq[i];
    }

    cout << endl;
    return 0;
}
