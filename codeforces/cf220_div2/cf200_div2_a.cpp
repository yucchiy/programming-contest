#include <iostream>
#include <cmath>

using namespace std;
#define INF 10000000

int n, m, i, j, a, b;

int dist(int x, int y)
{
    if ( ((int)abs(i - x) % a) == 0 && ((int)abs(j - y) % b) == 0 && ( ((int)abs(i - x) / a) % 2 ) == ( ((int)abs(j - y) / b) % 2 ) ) {
        return max((int)abs(i - x) / a, (int)abs(j - y) / b);
    }
    return INF;
}


int main()
{
    cin >> n >> m >> i >> j >> a >> b;

    if ((i == 1 || i == n) &&  (j == 1 || j == m)) {
        cout << 0 << endl;
        return 0;
    }

    if ( ((int)abs(i - n) < a && (int)abs(i - 1) < a) || ((int)abs(j - m) < b && (int)abs(j - 1) < b) ) {
        cout << "Poor Inna and pony!" << endl;
        return 0;
    }

    int res = INF;
    res = min(res, dist(1, 1));
    res = min(res, dist(1, m));
    res = min(res, dist(n, 1));
    res = min(res, dist(n, m));
    if (res == INF) {
        cout << "Poor Inna and pony!" << endl;
    } else {
        cout << res << endl;
    }

}    
