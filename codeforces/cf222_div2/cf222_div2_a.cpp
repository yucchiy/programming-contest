#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int a, b, c = 0, d = 0, e = 0;
    cin >> a >> b;
    for (int x = 1; x <= 6; x++) {
        int da = abs(a - x), db = abs(b - x);
        if (da < db) c++;
        else if (da == db) d++;
        else e++;
    }

    cout << c << " " << d << " " << e << endl;
}