#include <iostream>

using namespace std;

int main()
{
    int n, cards[1000], a = 0, b = 0, l, r;
    cin >> n;
    for (int i = 0; i < n; i++) cin >> cards[i];
    
    l = 0; r = n - 1;
    for (int i = 0; i < n; i++) {
        int num = 0;
        if (cards[l] < cards[r]) {
            num = cards[r];
            r--;
        } else {
            num = cards[l];
            l++;
        }

        if (i % 2 == 0) {
            a += num;
        } else {
            b += num;
        }
    }

    cout << a << " " << b << endl;
    return 0;
}
