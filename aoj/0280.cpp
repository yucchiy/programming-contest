#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int N;
    while (true) {
        cin >> N;
        if (N == 0) break;

        int cards[10] = {0}, ba = 0;
        char card;
        for (int i = 0; i < 100; i++) {
            cin >> card;
            if (card == 'S') {
                ba += cards[i % N] + 1;
                cards[i % N] = 0;
            } else if (card == 'L') {
                cards[i % N] += ba + 1;
                ba = 0;
            } else {
                cards[i % N]++;
            }

        }

        sort(cards, cards + N);
        for (int i = 0; i < N; i++) cout << cards[i] << " ";
        cout << ba << endl;
    }

    return 0;
}
