#include <iostream>
#include <string>

using namespace std;

int main()
{
    string s;
    cin >> s;

    int c;
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == '^') c = i;
    }

    long long l = 0, r = 0;
    for (int i = 0; i < s.length(); i++) {
        if (s[i] == '=' || s[i] == '^') {
            continue;
        } else {
            if (i < c) {
                l += (c - i) * (s[i] - '0');
            } else {
                r += (i - c) * (s[i] - '0');
            }
        }
    }

    if (l > r) {
        cout << "left" << endl;
    } else if (l == r) {
        cout << "balance" << endl;
    } else {
        cout << "right" << endl;
    }
}
