#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    int n, m, l[101];
    cin >> n >> m;

    for (int i = 0; i < 101; i++) {
        l[i] = 0;
    }

    for (int i = 0 ;i < m; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        l[a] += c;
        l[b] -= c;
    }

    int res = 0;
    for (int i = 1; i <= n; i++) {
        if (l[i] > 0) {
            res += l[i];
        }
    }

    cout << res << endl;
}
