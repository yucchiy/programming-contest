#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    int n, a[500000];
    cin >> n;
    for (int i = 0; i < n; i++) cin >> a[i];

    sort(a, a + n);
    int ans = n, j = n - 1;
    for (int i = 0; i < (n / 2); i++) {
        if (a[n / 2 - i - 1] * 2 <= a[j]) {
            ans--;
            j--;
        }
    }

    cout << ans << endl;
    return 0;
}
