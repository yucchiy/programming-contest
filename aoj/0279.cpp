#include <iostream>

using namespace std;

int main()
{
    int N;
    while (true) {
        cin >> N;
        if (N == 0) break;

        int cnt = 0, k;
        bool ok = false;
        for (int i = 0; i < N; i++) {
            cin >> k;
            if (k > 0) {
                cnt++;
                if (k > 1) ok = true;
            }
        }

        if (ok) 
            cout << cnt + 1 << endl;
        else
            cout << "NA" << endl;
    }
}


